package com.example.player

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    private val startService: Button by lazy { buttonStart }
    private val stopService: Button by lazy { buttonStop }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChanel()

        startService.setOnClickListener { startService() }
        stopService.setOnClickListener { stopService() }

    }

    private fun createNotificationChanel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(Const.CHANEL_ID,
                "location service", NotificationManager.IMPORTANCE_LOW)

            val manager: NotificationManager = getSystemService(NotificationManager::class.java)

            manager.createNotificationChannel(notificationChannel)
        }

    }

    private fun stopService() {

        stopService(Intent(this, ForegroundService::class.java))
    }

    private fun startService() {
        startService(Intent(this, ForegroundService::class.java))
    }


}
