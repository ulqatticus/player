package com.example.player

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.IBinder
import android.support.v4.app.NotificationCompat


class ForegroundService : Service() {

    private lateinit var player: MediaPlayer
    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {


        val notification = NotificationCompat.Builder(this, Const.CHANEL_ID)
            .setContentTitle("Music Service")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setBadgeIconType(R.mipmap.ic_launcher)
            .build()
        startForeground(1, notification)


        //getting systems default ringtone
        player = MediaPlayer()
        player.setDataSource(Const.STATION)
        player.prepare()
        //staring the player
        player.start()

        return START_STICKY
    }

    //stopping the player when service is destroyed
    override fun onDestroy() {
        super.onDestroy()
        player.stop()
    }
}